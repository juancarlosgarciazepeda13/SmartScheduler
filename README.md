# SmartScheduler
 Proyecto para la UEA de ADSI

El proyecto realizado en NetBeans se encuentra dentro de la carpeta llamada "Smart Scheduler" y ele ejecutable es la clase "Tester.java" que se encuentra en la ruta "SmartScheduler/Smart Scheduler/src/uam/azc/adsi/smartscheduler/controllers/Tester.java".

La caperta "Diagramas" contiene a los diagramas respectivos a los modelos de secuencia, modelos de robustez, y de clase.

Equipo "MachineGun Systems"

Integrantes:

* Juan Carlos García Zepeda - 2183033750
* Jesus Raymundo Hernandez García - 2123034026
* Octavio Ramirez Ramirez - 2132001731
